
This is a spring boot web project which tells if two cities are connected

To build the project standard maven build is sufficient mvn clean install

To start/run the project spring boot maven plugin is included just run  spring-boot:run

It deployed as a Spring Boot APP and expose one endpoint: 

http://localhost:1189/Connected?origin=Bloomington&destination=Normal

Customized the port in application.properties under resource folder

