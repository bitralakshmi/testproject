package com.example.demo.control;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.FileToMap;


@RestController
public class CityController {
	
	@Autowired
	private FileToMap fileToMap;
	
	@GetMapping("/Connected")
	public ResponseEntity<String> isConnected(
			@RequestParam(required =true, defaultValue ="") final String origin,
			@RequestParam(required =true, defaultValue ="") final String destination) throws IOException
	{
				return new ResponseEntity<String>(fileToMap.areCitiesConnected(origin,destination), HttpStatus.OK);
		
	}}
